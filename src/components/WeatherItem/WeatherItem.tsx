import React from 'react';

import { TFForecasts } from '~/types/weather';

import { formatDate } from '~/utils/formatDate';

interface TFProps {
  item: TFForecasts;
}

export const WeatherItem: React.FC<TFProps> = ({ item }) => {
  return (
    <div>
      <p>Минимальная температура: {item.parts.day.temp_min}</p>
      <p>Максимальная температура: {item.parts.day.temp_max}</p>
      <p>Средняя температура: {item.parts.day.temp_avg}</p>
      <p>Температура ощущается: {item.parts.day.feels_like}</p>
      <p>Дата: {formatDate(new Date(item.date), 'dd/MM//yyyy')}</p>
      <p>Описание: {item.parts.day.condition}</p>
      <p>Атмосферное давление: {item.parts.day.pressure_mm}</p>
      <p>Влажность: {item.parts.day.humidity}</p>
    </div>
  );
};
