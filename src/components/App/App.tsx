import React from 'react';
import { RouterProvider, createBrowserRouter, redirect } from 'react-router-dom';
import { useSelector } from 'react-redux';

import type { RootState } from '~/store/store';

import { WelcomePage } from '~/pages/WelcomePage';
import { WeatherForecastPage } from '~/pages/WeatherForecastPage';
import { WeatherForecastListPage } from '~/pages/WeatherForecastListPage';

import { isEmptyObject } from '~/utils/isEmptyObject';

import './styles/global.css';

export const App = () => {
  const isWeatherEmpty = useSelector((state: RootState) => {
    return isEmptyObject(state.weatherReducer.weather);
  });

  const router = createBrowserRouter([
    {
      path: '/',
      element: <WelcomePage />,
    },
    {
      path: '/weather',
      element: <WeatherForecastPage />,
      loader: () => {
        if (isWeatherEmpty) {
          return redirect('/');
        }
      },
    },
    {
      path: '/weather-list',
      element: <WeatherForecastListPage />,
      loader: () => {
        if (isWeatherEmpty) {
          return redirect('/');
        }
      },
    },
  ]);

  return <RouterProvider router={router} />;
};
