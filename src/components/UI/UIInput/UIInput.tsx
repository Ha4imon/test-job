import React from 'react';
import { Input, InputProps } from 'antd';

export const UIInput: React.FC<InputProps> = (props) => {
  return <Input {...props} />;
};
