import React from 'react';
import { Table, TableProps, TableColumnProps } from 'antd';

export type TFTableColumnProps<T> = TableColumnProps<T>;

export const UITable = <T extends Object>(props: React.PropsWithChildren<TableProps<T>>): JSX.Element => {
  return <Table<T> {...props} />;
};
