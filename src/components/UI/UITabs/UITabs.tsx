import React from 'react';
import { Tabs, TabsProps } from 'antd';

export const UITabs: React.FC<TabsProps> = (props) => {
  return <Tabs {...props} />;
};
