import React from 'react';
import { Button, ButtonProps } from 'antd';

export const UIButton: React.FC<ButtonProps> = (props) => {
  return <Button {...props} />;
};
