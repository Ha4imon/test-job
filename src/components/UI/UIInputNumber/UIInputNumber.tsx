import React from 'react';
import { InputNumber, InputNumberProps } from 'antd';

export const UIInputNumber: React.FC<InputNumberProps> = (props) => {
  return <InputNumber {...props} />;
};
