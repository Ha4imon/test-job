import React, { ChangeEvent, useState } from 'react';

import { TFPosition } from '~/types/position';

import { UIButton } from '~/components/UI/UIButton';
import { UIInput } from '~/components/UI/UIInput';
import { UIInputNumber } from '~/components/UI/UIInputNumber';
import { UITable } from '~/components/UI/UITable';

import { createTableConfig } from './createTableConfig';
import style from './NewPositionForm.module.css';

interface TFProps {
  activePosition: TFPosition;
  positionsList: TFPosition[];
  submitFormHandler: (name: string, latitude: number, longitude: number) => void;
  activatePositionHander: (id: number) => void;
  onClearClick: () => void;
}

const initialFields = {
  name: '',
  longitude: '',
  latitude: '',
};

type initialFieldsKeys = keyof typeof initialFields;

export const NewPositionForm: React.FC<TFProps> = ({
  activePosition,
  positionsList,
  submitFormHandler,
  activatePositionHander,
  onClearClick,
}) => {
  const tableConfig = createTableConfig(activePosition, activatePositionHander);

  const [fieldsObject, setFieldsObject] = useState<{ [key in initialFieldsKeys]: string | number }>(initialFields);

  const changeFieldValue = (event: ChangeEvent<HTMLInputElement>) => {
    setFieldsObject({ ...fieldsObject, [event.target.name]: event.target.value });
  };

  const changePositionValue = (value: number | string | null, filed: string) => {
    setFieldsObject({ ...fieldsObject, [filed]: value });
  };

  const formSubmitHandler = () => {
    submitFormHandler(fieldsObject.name as string, Number(fieldsObject.latitude), Number(fieldsObject.longitude));
    setFieldsObject(initialFields);
  };

  return (
    <div className={style.container}>
      <div className={style.formWrapper}>
        <label>
          Введите название
          <UIInput name='name' placeholder='Название' value={fieldsObject.name} onInput={changeFieldValue} />
        </label>
        <label>
          Введите широту
          <UIInputNumber
            style={{ width: '100%' }}
            type='number'
            placeholder='53.9231'
            min='-90'
            max='90'
            name='latitude'
            value={fieldsObject.latitude}
            onChange={(value: number | string | null) => {
              changePositionValue(value, 'latitude');
            }}
          />
        </label>
        <label>
          Введите долготу
          <UIInputNumber
            style={{ width: '100%' }}
            type='number'
            placeholder='53.2333'
            min='-180'
            max='180'
            name='longitude'
            value={fieldsObject.longitude}
            onChange={(value: number | string | null) => {
              changePositionValue(value, 'longitude');
            }}
          />
        </label>
        <UIButton onClick={formSubmitHandler} type='primary'>
          Добавить
        </UIButton>
        <UIButton danger onClick={onClearClick}>
          Удалить добавленные
        </UIButton>
      </div>
      <UITable<TFPosition>
        size='middle'
        rowKey='id'
        columns={tableConfig}
        dataSource={positionsList}
        pagination={false}
        scroll={{ y: '300px', x: '0' }}
      />
    </div>
  );
};
