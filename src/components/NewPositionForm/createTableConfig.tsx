import React from 'react';

import { TFPosition } from '~/types/position';
import type { TFTableColumnProps } from '~/components/UI/UITable';

import { UIButton } from '~/components/UI/UIButton';

export const createTableConfig = (
  activePosition: TFPosition,
  onButtonClick: (id: number) => void
): TFTableColumnProps<TFPosition>[] => {
  return [
    {
      title: 'Название',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'Широта',
      dataIndex: 'latitude',
      key: 'latitude',
      align: 'center',
    },
    {
      title: 'Долгота',
      dataIndex: 'longitude',
      key: 'longitude',
      align: 'center',
    },
    {
      title: '',
      key: 'btn',
      align: 'center',
      render: ({ id }: TFPosition) =>
        activePosition.id === id ? (
          'Выбрано'
        ) : (
          <UIButton
            type='primary'
            onClick={() => {
              onButtonClick(id);
            }}
          >
            Выбрать
          </UIButton>
        ),
    },
  ];
};
