import React, { useEffect, useLayoutEffect, useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';

import type { RootState } from '~/store/store';
import { TFAvailableCodeError } from '~/types/position';

import {
  savePosition,
  saveError,
  activatePosition,
  clearPositionListWithoutFirst,
  savePositionList,
} from '~/store/slices/positionSlice';
import { featchWeather } from '~/store/slices/weatherSlice';

import { geolocation } from '~/utils/geolocation';

import { UIButton } from '~/components/UI/UIButton/';
import { NewPositionForm } from '~/components/NewPositionForm';

import style from './WelcomePage.module.css';

let messageErrorMap = {
  1: 'Не удалось получить информацию о геолокации, поскольку у страницы не было на это разрешения.',
  2: 'Не удалось получить данные о местоположени',
  3: 'Время истекло до того, как информация была получена.',
};

export const WelcomePage: React.FC = () => {
  const { positionsList, activePosition, error } = useSelector((state: RootState) => state.positionReducer);

  const dispatch = useDispatch();

  useLayoutEffect(() => {
    if (!activePosition && !window.localStorage.getItem('positionsList')) {
      const onSuccess = (geo: GeolocationPosition) => {
        dispatch(
          savePosition({ latitude: geo.coords.latitude, longitude: geo.coords.longitude, name: 'Ваша геопозиция' })
        );
        dispatch(activatePosition(0));
      };

      const onError = (error: GeolocationPositionError) => {
        dispatch(saveError(error.code as TFAvailableCodeError));
      };

      geolocation(onSuccess, onError);
    } else if (window.localStorage.getItem('positionsList')) {
      dispatch(savePositionList(JSON.parse(window.localStorage.getItem('positionsList') as string)));
      dispatch(activatePosition(0));
    }
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    if (activePosition) {
      featchWeather({
        latitude: activePosition.latitude,
        longitude: activePosition.longitude,
      })(dispatch);
    }
  }, [activePosition, dispatch]);

  const saveNewPosition = useCallback(
    (name: string, latitude: number, longitude: number) => {
      dispatch(savePosition({ name, latitude, longitude }));
    },
    [dispatch]
  );

  const activatePositionHander = useCallback(
    (id: number) => {
      dispatch(activatePosition(id));
    },
    [dispatch]
  );

  const clearPositionHandler = () => {
    dispatch(clearPositionListWithoutFirst());
    dispatch(activatePosition(0));
  };

  return (
    <div className={style.container}>
      {!activePosition && !error && <p>Разрешите геолокацию для продолжения :)</p>}
      {error && (
        <div>
          <p>{messageErrorMap[error]} :(</p>
          <p>Разрешить доступ к данными геопозиции</p>
        </div>
      )}
      {activePosition && (
        <>
          <div className={style.weatherWrapper}>
            <p>
              Какой прогноз погоды хотите увидеть для <b>{activePosition.name}</b>?
            </p>
            <div className={style.btnWrapper}>
              <UIButton type='link'>
                <Link to='/weather'>Прогноз погоды сегодня</Link>
              </UIButton>
              <UIButton type='link'>
                <Link to='/weather-list'>Прогноз погоды на 7 дней</Link>
              </UIButton>
            </div>
          </div>
          <hr />
          <NewPositionForm
            activePosition={activePosition}
            positionsList={positionsList}
            submitFormHandler={saveNewPosition}
            onClearClick={clearPositionHandler}
            activatePositionHander={activatePositionHander}
          />
        </>
      )}
    </div>
  );
};
