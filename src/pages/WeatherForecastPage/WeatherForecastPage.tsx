import React from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';

import type { RootState } from '~/store/store';

import { formatDate } from '~/utils/formatDate';

import { UIButton } from '~/components/UI/UIButton/';

import style from './WeatherForecastPage.module.css';

export const WeatherForecastPage: React.FC = () => {
  const weatherData = useSelector((state: RootState) => {
    return {
      lon: state.weatherReducer.weather.info.lon,
      lat: state.weatherReducer.weather.info.lat,
      now_dt: formatDate(new Date(state.weatherReducer.weather.now_dt), 'HH:mm'),
      locality: state.weatherReducer.weather.geo_object,
      temp: state.weatherReducer.weather.fact.temp,
      feels_like: state.weatherReducer.weather.fact.feels_like,
      condition: state.weatherReducer.weather.fact.condition,
      wind_speed: state.weatherReducer.weather.fact.wind_speed,
      pressure_mm: state.weatherReducer.weather.fact.pressure_mm,
      humidity: state.weatherReducer.weather.fact.humidity,
    };
  });

  return (
    <div className={style.container}>
      <UIButton className={style.link} type='link'>
        <Link to='/'>← На главную</Link>
      </UIButton>
      <p>Долгота: {weatherData.lon}</p>
      <p>Широта: {weatherData.lat}</p>
      <p>
        Название местности:{' '}
        {weatherData.locality?.province?.name ||
          weatherData.locality?.locality?.name ||
          weatherData.locality?.district?.name ||
          weatherData.locality?.country?.name}
      </p>
      <p>Время: {weatherData.now_dt}</p>
      <p>Температура: {weatherData.temp}</p>
      <p>Ощущается: {weatherData.feels_like}</p>
      <p>Описание: {weatherData.condition}</p>
      <p>Скорость ветра: {weatherData.wind_speed}</p>
      <p>Давление: {weatherData.pressure_mm}</p>
      <p>Влажность: {weatherData.humidity}</p>
    </div>
  );
};
