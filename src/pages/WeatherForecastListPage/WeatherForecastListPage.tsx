import React from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';

import type { RootState } from '~/store/store';

import { formatDate } from '~/utils/formatDate';
import { uniqueId } from '~/utils/uniqId';

import { UITabs } from '~/components/UI/UITabs/UITabs';
import { UIButton } from '~/components/UI/UIButton/';
import { WeatherItem } from '~/components/WeatherItem';

import style from './WeatherForecastListPage.module.css';

export const WeatherForecastListPage: React.FC = () => {
  const weatherListData = useSelector((state: RootState) => {
    return state.weatherReducer.weather.forecasts.map((item) => {
      return {
        label: formatDate(new Date(item.date), 'dd/MM//yyyy'),
        key: uniqueId('weather-tab'),
        children: <WeatherItem item={item} />,
      };
    });
  });

  return (
    <div className={style.container}>
      <UIButton className={style.link} type='link'>
        <Link to='/'>← На главную</Link>
      </UIButton>
      <UITabs items={weatherListData} />
    </div>
  );
};
