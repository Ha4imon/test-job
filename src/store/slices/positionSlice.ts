import { createSlice } from '@reduxjs/toolkit';
import type { PayloadAction } from '@reduxjs/toolkit';

import { TFPosition, TFAvailableCodeError } from '~/types/position';

interface TFInitialState {
  activePosition?: TFPosition;
  positionsList: TFPosition[];
  error?: TFAvailableCodeError;
}

const initialState: TFInitialState = {
  error: undefined,
  activePosition: undefined,
  positionsList: [],
};

const positionSlice = createSlice({
  name: 'position',
  initialState,
  reducers: {
    activatePosition(state, action: PayloadAction<number>) {
      state.activePosition = state.positionsList[action.payload];
    },
    savePosition(state, action: PayloadAction<Omit<TFPosition, 'id'>>) {
      state.positionsList.push({ ...action.payload, id: state.positionsList.length });
      localStorage.setItem('positionsList', JSON.stringify(state.positionsList));
    },
    savePositionList(state, action: PayloadAction<TFPosition[]>) {
      state.positionsList = [...action.payload];
    },
    saveError(state, action: PayloadAction<TFAvailableCodeError>) {
      state.error = action.payload;
    },
    clearPositionListWithoutFirst(state) {
      state.positionsList = [state.positionsList[0]];
      localStorage.setItem('positionsList', JSON.stringify([state.positionsList[0]]));
    },
  },
});

export default positionSlice.reducer;
export const { savePosition, saveError, activatePosition, clearPositionListWithoutFirst, savePositionList } =
  positionSlice.actions;
