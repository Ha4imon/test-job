import { createSlice, Dispatch } from '@reduxjs/toolkit';

import type { PayloadAction } from '@reduxjs/toolkit';
import { TFWeather } from '~/types/weather';
import { TFPosition } from '~/types/position';

import { WeatherForecastService } from '~/services/WeatherForecastService';

export const featchWeather = (activePosition: Omit<TFPosition, 'id' | 'name'>) => async (dispatch: Dispatch) => {
  const data = await WeatherForecastService.get({
    latitude: activePosition.latitude,
    longitude: activePosition.longitude,
  });

  if (data) {
    dispatch(saveWeatherData(data));
  } else {
    dispatch(removeWeatherData());
  }
};

const initialState = {
  weather: {},
} as { weather: TFWeather };

const weatherSlice = createSlice({
  name: 'weather',
  initialState,
  reducers: {
    saveWeatherData(state, action: PayloadAction<TFWeather>) {
      state.weather = action.payload;
    },
    removeWeatherData(state) {
      state.weather = initialState.weather;
    },
  },
});

export default weatherSlice.reducer;
export const { saveWeatherData, removeWeatherData } = weatherSlice.actions;
