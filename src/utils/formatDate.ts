import { format } from 'date-fns';

export const formatDate = (date: Date, formatDate: string): string => {
  return format(date, formatDate);
};
