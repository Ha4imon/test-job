const idCounter: any = {};

export const uniqueId = (prefix = '_uniq') => {
  if (!idCounter[prefix]) {
    idCounter[prefix] = 0;
  }

  const id = ++idCounter[prefix];
  if (prefix === '_uniq') {
    return `${id}`;
  }

  return `${prefix}${id}`;
};
