export const geolocation = (
  onSuccess: (position: GeolocationPosition) => void,
  onError: (error: GeolocationPositionError) => void
) => {
  navigator.geolocation.getCurrentPosition(onSuccess, onError);
};
