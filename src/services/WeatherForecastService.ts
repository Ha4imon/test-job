import { api } from './api';

import { TFPosition } from '~/types/position';
import { TFWeather } from '~/types/weather';

export const WeatherForecastService = {
  get: async (params: Omit<TFPosition, 'id' | 'name'>) => {
    try {
      const {
        data: { ...items },
      } = await api.get<TFWeather>('/', {
        params: { lat: params.latitude, lon: params.longitude },
      });

      return items;
    } catch (e) {
      console.log(e);
    }
  },
};
