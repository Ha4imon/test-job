export interface TFInfo {
  lat: number;
  lon: number;
}

export interface TFGeoObj {
  country: { id: number; name: string };
  district: { id: number; name: string };
  locality: {
    id: number;
    name: string;
  };
  province: {
    id: number;
    name: string;
  };
}

enum TFCondition {
  'clear',
  'partly-cloudy',
  'cloudy',
  'overcast',
  'drizzle',
  'light-rain',
  'rain',
  'moderate-rain',
  'heavy-rain',
  'continuous-heavy-rain',
  'showers',
  'wet-snow',
  'light-snow',
  'snow',
  'snow-showers',
  'hail',
  'thunderstorm',
  'thunderstorm-with-rain',
  'thunderstorm-with-hail',
}

export interface TFFact {
  temp: number;
  feels_like: number;
  condition: TFCondition;
  wind_speed: number;
  pressure_mm: number;
  humidity: number;
}

export interface TFForecasts {
  date: string;
  parts: {
    day: {
      temp_min: number;
      temp_max: number;
      temp_avg: number;
      feels_like: number;
      condition: TFCondition;
      humidity: number;
      pressure_mm: number;
    };
  };
}

export interface TFWeather {
  geo_object: TFGeoObj;
  info: TFInfo;
  fact: TFFact;
  forecasts: TFForecasts[];
  now_dt: string;
}
