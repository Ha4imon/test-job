export interface TFPosition {
  id: number;
  latitude: number;
  longitude: number;
  name: string;
}

export type TFAvailableCodeError = 1 | 2 | 3;
