const path = require('path');

module.exports = {
  webpack: {
    alias: {
      '~': path.resolve(__dirname, 'src/'),
    },
  },
  typescript: {
    enableTypeChecking: true,
  },
  devServer: {
    open: false,
    client: {
      overlay: false,
    },
  },
};
